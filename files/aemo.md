# Manual to reconstruct the data used in aemo.ipynb

1. Follow the [link](https://visualisations.aemo.com.au/aemo/nemweb/index.html#mms-data-model)

2. Now **for every month of the Years 2023-2010**

- Under "Download CSV" select csv-Button

- In the pop-up page there will be a lot of CSV-files. **Search for "SCADA" (STRG+F)** and download only this file

4. Write code to merges the CSV's different months accoring to the windfarm initials (like CAPTL_WF CATHROCK ...) where the timestep is the first column

5. As the code opens the data like this: "../../data/raw/aemo/" unzipp in a new folder save under the notebooks accoringly to the mentioned path
