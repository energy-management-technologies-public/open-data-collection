# Manual to reconstruct the data used in Charging_power_measurements.ipynb

1. Follow the [link](https://zenodo.org/records/10184777)
2. You can download the .csv file by clicking on the "download"-field in the "Files" section
3. Drag the .csv file to the folder ".\\open-data-collection\\files"
4. Install the requirements within the requirements.txt
5. Run the Jupyter Notebook `Charging_power_measurements.ipynb`
