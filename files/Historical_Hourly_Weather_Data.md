# Manual to reconstruct the data used in Historical_Hourly_Weather_Data.ipynb

1. Follow the [link](https://www.kaggle.com/datasets/selfishgene/historical-hourly-weather-data/data?select=wind_direction.csv)
2. You can download the .csv file by clicking on the "download"-field
3. Drag the .csv file to the folder ".\\open-data-collection\\files"
4. Install the requirements within the requirements.txt
5. Run the Jupyter Notebook `Historical_Hourly_Weather_Data.ipynb`
