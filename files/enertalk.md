# Manual to reconstruct the data used in enertalk.ipynb

1. Follow the [link](https://springernature.figshare.com/articles/dataset/House_05_in_ENERTALK_dataset/8123543?backTo=/collections/The_ENERTALK_Dataset_15_Hz_Electricity_Consumption_Data_from_22_Houses_in_Korea/4502780)

2. Click on the download button on the bottom left side of the page 1.45 GB, (you might need to scoll a bit down to see it)

3. As the code reads the data like this: "../../files/ENERTALK/05" unzipp in a new folder save under the notebooks accoringly to the mentioned path
