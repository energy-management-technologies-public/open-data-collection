# Manual to reconstruct the data used in ecmwf.ipynb

1. Follow the [link](http://data.aicnic.cn/ECMWF/)

2. Now search for the used dataset: **ECMWF_HRES_12Z_20201231.nc**

3. The data was read and processed using the netCDF4 library (the loading is already implemented but here the [documentation](https://unidata.github.io/netcdf4-python/))
   '''
   pip install netCDF4
   '''

4. As the code read the data like this: ""../files/EMCWF/ECMWF_HRES_12Z_20201231.nc"" save under the notebooks accoringly to the mentioned path
