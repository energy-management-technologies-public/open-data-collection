# Manual to reconstruct the data used in deddiag.ipynb

1. Follow the [link](https://figshare.com/articles/dataset/DEDDIAG_a_domestic_electricity_demand_dataset_of_individual_appliances_in_Germany/13615073?file=26179646) for the data source for House 05

2. Download everthing the whole data (14,6 GB)

3. As the code opens the data like this: '../files/DEDDIAG/items.tsv' unzipp in a new folder save under the notebooks accoringly to the mentioned path
