# Manual to reconstruct the data used in care.ipynb

1. Follow the [link](https://zenodo.org/records/14006163)

2. Download the `.zip` archive and extract it.

3. Extract the `Wind Farm A.zip` archive and move it to `files/care/Wind Farm A`
