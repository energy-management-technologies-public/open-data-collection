# Manual to reconstruct the data used in individual_houshold.ipynb

1. Follow the [link](https://archive.ics.uci.edu/dataset/235/individual+household+electric+power+consumption)

2. Download the zip folder and extract the text document named: 'household_power_consumption.txt'

3. Save the text Document in a relative path to the notebook such as this '../files/Household_electric_consumption/household_power_consumption.txt'
