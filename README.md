# Open Data Collection

This repository hosts the exploratory data analysis (EDA) jupyter notebooks for the [TUM-EMT Open Data Collection](https://collab.dvb.bayern/display/TUMenmantech/TUM-EMT+Open+Data+Collection).

The main aim of this repository is to save the time researchers usually need to explore data and accelerate energy research 🚀. Here, we cover

- AEMO [[1]](#1), which is free to use for any purpose, when accurately and appropriately attributing AEMO as the author, see the [AEMO Copyright Permissions](https://www.aemo.com.au/energy-systems/electricity/national-electricity-market-nem/data-nem/market-data-nemweb),
- Battery System with Subunits [[2]](#2) (license: CC BY 4.0),
- DEDDIAG [[3]](#3) (license: CC BY 4.0),
- Distribution Grid Simulation [[4]](#4) (license: Open Data Common Open Database Licence),
- ECMWF [[5]](#5) (license: CC BY 4.0),
- ECMWF Solar [[6]](#6) (license: CC BY 4.0),
- ENERTALK [[7]](#7) (license: CC0 1.0),
- GGV Load [[8]](#8) (currently under maintenance),
- Individual Household Load [[9]](#9) (license: CC BY 4.0),
- Kelmarsh [[10]](#10) (license: CC BY 4.0).
- Penmanshiel Wind Farm Data [[11]](#11) (license: CC BY 4.0).
- CARE to Compare [12](#12) (license: CC BY-SA 4.0).

We do not provide the data files themselves, but more information on the data, how they are licensed and where they can be downloaded can be found in the TUM-EMT Open Data Collection.

This repository is part of the Energy Data Lab project, an initiative to accelerate energy research via providing open-source benchmark data sets and (pre-trained) models. Find out more [here](https://www.epe.ed.tum.de/emt/forschung/forschungsprojekte/energy-data-lab/)!

## Getting Started

To run the notebooks yourself, the required datasets have to be downloaded first at the corresponding source (you can find the sources in the TUM-EMT Open Data Collection).

For most notebooks, it then is enough to create a virtual environment and run

```
pip install -r requirements.txt
```

In case you want to run those EDA notebooks, where you need the Non-Intrusive Load Monitoring Toolkit (NILMTK), the installation is a bit more tedious. We recommend to first follow the installation instructions explained [here](https://github.com/nilmtk/nilmtk/blob/master/docs/manual/user_guide/install_user.md), and then run

```
pip install git+https://github.com/nilmtk/nilm_metadata
```

in your console. Any other dependency must be installed carefully to not create package conflicts. For the UK-DALE and REDD notebook, we recommend to go with

```
pip install jupyter
pip install seaborn --no-deps
```

## License

This repository is licensed unter the [MIT License](https://gitlab.lrz.de/energy-management-technologies-public/open-data-collection/-/blob/main/LICENSE).

## References

<a id="1">[1]</a>
AEMO. 2024. Nemweb Archive Reports. National Electricity Market Web. https://nemweb.com.au/Reports/Archive/

<a id="2">[2]</a>
Steinbuß G, Rzepka B, Bischof S, Blank T, Böhm, K. Frequent Observations from a Battery System with Subunits. 2019. https://doi.org/10.5445/IR/1000094469

<a id="3">[3]</a>
Wenninger M, Maier A, Schmidt J. DEDDIAG, a domestic electricity demand dataset of individual appliances in Germany. Scientific Data. 2021; 8(1). https://doi.org/10.1038/s41597-021-00963-2

<a id="4">[4]</a>
Meinecke S, Sarajlić D, Drauz SR, Klettke A, Lauven L-P, Rehtanz C, Moser A, Braun M. SimBench—A Benchmark Dataset of Electric Power Systems to Compare Innovative Solutions Based on Power Flow Analysis. Energies. 2020; 13(12). https://doi.org/10.3390/en13123290

<a id="5">[5]</a>
Dazhi Y, Wenting W, Tao H. A historical weather forecast dataset from the European Centre for Medium-Range Weather Forecasts (ECMWF) for energy forecasting. Solar Energy. 2022; 232. https://doi.org/10.1016/j.solener.2021.12.011

<a id="6">[6]</a>
Wenting W, Dazhi Y, Tao H, Jan K. An archived dataset from the ECMWF Ensemble Prediction System for probabilistic solar power forecasting. Solar Energy. 2022; 248. https://doi.org/10.1016/j.solener.2022.10.062

<a id="7">[7]</a>
Shin C, Lee E, Han J, Yim J, Rhee W, Lee H. The ENERTALK dataset, 15 Hz electricity consumption data from 22 houses in Korea. Scientific Data. 2019; 6(1). https://doi.org/10.1038/s41597-019-0212-5

<a id="8">[8]</a>
Stadtwerke Groß-Gerau Versorgungs GmbH. Lastprofile. 2020. https://www.ggv-energie.de/cms/netz/allgemeine-daten/netzbilanzierung-download-aller-profile.php

<a id="9">[9]</a>
Hebrail G, Berard A. Individual Household Electric Power Consumption. UCI Machine Learning Repository. 2012. https://doi.org/10.24432/C58K54

<a id="10">[10]</a>
Plumley C. Kelmarsh wind farm data (0.1.0). Zenodo. 2022. https://doi.org/10.5281/zenodo.8252025

<a id="11">[11]</a>
Plumley C. Penmanshiel Wind Farm Data. 0.1.0. Zenodo. 2022. https://doi.org/10.5281/zenodo.8253010

<a id="12">[12]</a>
Gück C, Roelofs C. CARE to Compare. Zenodo. 2024. https://doi.org/10.5281/zenodo.14006163

